This is a project that get data from json.placeholder.api and display it using immer, react and typesccript.

I used useCallback and memo in order to prevent unecessary rerender of react components
![Image of Profiler](/public/profiler.png)

Immer library is a powerful tool to work with imutable data. In this project I applied techniques like producer, curring producer, useImmer and useReducer.