export interface UserInterface {
    readonly name: string
    readonly id: number | null
}

export interface PostInterface {
    readonly title: string
    readonly id: number | null
}

export interface TodoInterface {
    readonly title: string
    readonly id: number
}

export interface State {
    readonly users: readonly UserInterface[]
    readonly todos: readonly TodoInterface[]
    readonly posts: readonly PostInterface[]
}

export const initialState = {
  users: [{
    id: 102937239892,
    name: 'first name',
  }],
  posts: [{
    id: 102937239892,
    title: 'first post',
  }],
  todos: [{
    id: 102937239892,
    title: 'first todo',
  }],
};
