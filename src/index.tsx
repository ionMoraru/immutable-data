import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';

import { Header } from './components/Header';
import { Users } from './scenes/users/UsersScene';
import { Todos } from './scenes/todos/TodosScene';
import { Posts } from './scenes/posts/PostsScene';

ReactDOM.render(
  <Router basename="/immutable-data">
    <Header />
    <Switch>
      <Route exact path="/" children={<Users />} />
      <Route exact path="/posts" children={<Posts />} />
      <Route exact path="/todos" children={<Todos />} />
    </Switch>
  </Router>,
  document.getElementById('root'),
);
