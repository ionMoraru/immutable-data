import { UserInterface } from '../../initialState';

export const fetchUser = async (id: number): Promise<UserInterface> => {
  const data = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`);
  const user = await data.json();
  return user.name;
};
