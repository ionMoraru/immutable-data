import produce, { Draft } from 'immer';
import { State, UserInterface } from '../../initialState';

export const setUsers = produce((draft: Draft<State>, users: UserInterface[]) => {
  const newArray = draft.users.concat(users);
  draft.users = newArray;
});

export const addUser = produce((draft: Draft<State>, name: string) => {
  draft.users.push({ name, id: 10 });
});


export const removeUser = produce((draft: Draft<State>, id: number) => {
  const users = draft.users.filter((user: UserInterface) => user.id !== id);
  draft.users = users;
});


export const renameUser = produce((draft: Draft<State>, id: number, name: string) => {
  const user = draft.users.find((user: UserInterface) => user.id === id);
  if (!user) return;
  user.name = name;
});
