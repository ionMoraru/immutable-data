import produce, { Draft } from 'immer';
import { State, PostInterface, initialState } from '../../initialState';

export const postsReducer = produce((draft: Draft<State>, action) => {
  switch (action.type) {
    case 'SET_POSTS':
      const newArray = draft.posts.concat(action.posts);
      draft.posts = newArray;
      break;
    case 'ADD_POST': {
      const { title, id } = action;
      draft.posts.push({ title, id });
      break;
    }
    case 'REMOVE_POST': {
      const { id } = action;
      const posts = draft.posts.filter((post: PostInterface) => post.id !== id);
      draft.posts = posts;
      break;
    }
    case 'RENAME_POST': {
      const { id, title } = action;
      const post = draft.posts.find((post: PostInterface) => post.id === id);
      if (!post) return;
      post.title = title;
      break;
    }
    case 'RESET_POSTS':
      return initialState;
  }
});
