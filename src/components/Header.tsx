import React from 'react';
import { Link } from 'react-router-dom';

export const Header: React.FC<{}> = () => (
  <div style={{ display: 'flex', justifyContent: 'space-around', marginBottom: 20 }}>
    <Link to="/">Users</Link>
    <Link to="/posts">Posts</Link>
    <Link to="/todos">Todos</Link>
  </div>
);
