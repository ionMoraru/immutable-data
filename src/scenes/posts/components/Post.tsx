import React, { memo } from 'react';
import { PostInterface } from '../../../initialState';

export interface Props {
    post: PostInterface;
    onRenamePost: (id: number | null, title: string) => void;
    onRemovePost: (id: number | null) => void;
}

export const Post: React.FC<Props> = memo(
  ({ post: { title, id }, onRemovePost, onRenamePost }) => (
    <li style={{ display: 'flex' }}>
      <span onClick={() => onRenamePost(id, 'renamed')}>
        {title}
      </span>
      <span style={{ marginLeft: 20, cursor: 'pointer' }} onClick={() => onRemovePost(id)}> x </span>
    </li>
  ),
);
