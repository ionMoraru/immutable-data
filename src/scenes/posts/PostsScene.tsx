import React, {
  useEffect, useReducer, Reducer, useCallback,
} from 'react';
import { Post } from './components/Post';
import { initialState, State } from '../../initialState';
import { postsReducer } from '../../services/posts/reducer';

export function Posts() {
  const [state, dispatch] = useReducer<Reducer<State, any>>(postsReducer, initialState);
  const { posts } = state;

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/posts')
      .then((response) => response.json())
      .then((json) => dispatch({
        type: 'SET_POSTS',
        posts: json,
      }));
  }, []);

  const onAddPost = () => {
    const title = prompt('Add title');
    if (!title) return;
    dispatch({
      type: 'ADD_POST',
      id: 2,
      title,
    });
  };

  const onRemovePost = useCallback((id: number | null) => {
    if (id) {
      dispatch({
        type: 'REMOVE_POST',
        id,
      });
    }
  }, []);

  const onRenamePost = useCallback((id, title) => {
    if (id && title) {
      dispatch({
        type: 'RENAME_POST',
        id,
        title,
      });
    }
  }, []);

  const onReset = () => {
    dispatch({
      type: 'RESET_POSTS',
    });
  };


  return (
    <>
      <button onClick={onAddPost}>Add new post title</button>
      <button onClick={onReset}>Reset</button>
      <ul>
        {posts.map((post) => <Post post={post} onRemovePost={onRemovePost} onRenamePost={onRenamePost} />)}
      </ul>
    </>
  );
}
