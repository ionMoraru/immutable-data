import React, { useEffect, useState, useCallback } from 'react';
import { User } from './components/User';
import { initialState, State } from '../../initialState';
import {
  setUsers, addUser, removeUser, renameUser,
} from '../../services/users/producer';
import { fetchUser } from '../../services/users/api';


export const Users: React.FC = () => {
  const [state, setState] = useState<State>(initialState);
  const { users } = state;

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then((response) => response.json())
      .then((json) => setState(setUsers(initialState, json)));
  }, []);

  const onAddUser = () => {
    const name = prompt('Add name');
    if (name) setState((state) => addUser(state, name));
  };

  const onRemoveUser = useCallback((id) => {
    if (id) setState((state) => removeUser(state, id));
  }, []);

  const onRenameUser = useCallback((id, name) => {
    if (id && name) setState((state) => renameUser(state, id, name));
  }, []);

  const onFetchUser = async () => {
    const id = Number(prompt('Add one id from 1 to 10'));
    const name = await fetchUser(id);

    if (name) setState((state) => addUser(state, String(name)));
  };

  return (
    <>
      <button onClick={onAddUser}>Add new user</button>
      <button onClick={onFetchUser}>Add new user by id</button>
      <ul>
        {users.map((user) => <User user={user} onRenameUser={onRenameUser} onRemoveUser={onRemoveUser} />)}
      </ul>
    </>
  );
};
