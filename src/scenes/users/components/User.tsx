import React, { memo } from 'react';
import { UserInterface } from '../../../initialState';

export interface Props {
    user: UserInterface;
    onRenameUser: (id: number | null, title: string) => void;
    onRemoveUser: (id: number | null) => void;
}

export const User: React.FC<Props> = memo(
  ({ user: { name, id }, onRenameUser, onRemoveUser }) => (
    <li style={{ display: 'flex' }}>
      <span onClick={() => onRenameUser(id, 'renamed')}>
        {' '}
        {name}
        {' '}
      </span>
      <span style={{ marginLeft: 20, cursor: 'pointer' }} onClick={() => onRemoveUser(id)}> x </span>
    </li>
  ),
);
