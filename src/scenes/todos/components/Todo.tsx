import React, { memo } from 'react';
import { TodoInterface } from '../../../initialState';

export interface Props {
    todo: TodoInterface;
    onRenameTodo: (id: number | null, title: string) => void;
    onRemoveTodo: (id: number | null) => void;
}

export const Todo: React.FC<Props> = memo(
  ({ todo: { title, id }, onRenameTodo, onRemoveTodo }) => (
    <li style={{ display: 'flex' }}>
      <span onClick={() => onRenameTodo(id, 'renamed')}>
        {title}
      </span>
      <span style={{ marginLeft: 20, cursor: 'pointer' }} onClick={() => onRemoveTodo(id)}> x </span>
    </li>
  ),
);
