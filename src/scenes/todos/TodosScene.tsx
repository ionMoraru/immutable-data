import React, { useEffect, useCallback } from 'react';
import { useImmer } from 'use-immer';
import { Todo } from './components/Todo';
import { initialState, State } from '../../initialState';

export function Todos() {
  const [state, updateState] = useImmer<State>(initialState);
  const { todos } = state;

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/todos')
      .then((response) => response.json())
      .then((json) => updateState((draft) => {
        const newArray = draft.todos.concat(json);
        draft.todos = newArray;
      }));
  }, [updateState]);

  const onAddTodo = () => {
    const title = prompt('Add title');
    if (title) {
      updateState((draft) => {
        draft.todos.push({ title, id: 1 });
      });
    }
  };

  const onRemoveTodo = useCallback((id) => {
    if (id) {
      updateState((draft) => {
        const todos = draft.todos.filter((todo) => todo.id !== id);
        draft.todos = todos;
      });
    }
  }, [updateState]);

  const onRenameTodo = useCallback((id, title) => {
    if (id && title) {
      updateState((draft) => {
        const todo = draft.todos.find((todo) => todo.id === id);
        if (!todo) return;
        todo.title = title;
      });
    }
  }, [updateState]);

  const onReset = () => {
    updateState(() => initialState);
  };

  return (
    <>
      <button onClick={onAddTodo}>Add new todo</button>
      <button onClick={onReset}>Reset</button>
      <ul>
        {todos.map((todo) => <Todo todo={todo} key={todo.id} onRenameTodo={onRenameTodo} onRemoveTodo={onRemoveTodo} />)}
      </ul>
    </>
  );
}
